Source: bumblebee-status
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ben Westover <me@benthetechguy.net>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-rtd-theme,
 python3-tk
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/python-team/packages/bumblebee-status.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/bumblebee-status
Homepage: https://github.com/tobi-wan-kenobi/bumblebee-status
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python

Package: bumblebee-status
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, ${shlibs:Depends}
Recommends: i3-wm, fonts-font-awesome, fonts-powerline
Description: Modular, themeable status line generator for the i3 window manager
 bumblebee-status is a status line generator for the i3 window manager.
 .
 Focus is on:
  * ease of use, sane defaults (no mandatory configuration file)
  * easy creation of custom themes
  * easy creation of custom modules

Package: bumblebee-status-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Multi-Arch: foreign
Description: Status line generator for the i3 window manager - documentation
 bumblebee-status is a status line generator for the i3 window manager.
 .
 Focus is on:
  * ease of use, sane defaults (no mandatory configuration file)
  * easy creation of custom themes
  * easy creation of custom modules
 .
 This package contains the documentation for the bumblebee-status package.
